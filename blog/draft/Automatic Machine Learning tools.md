# Automatic Machine Learning

## Rapid Miner

Unsupervised learning
For example dataset titled "income prediction" with education level, age, and demographics (not income) Rapidminder found a "pattern" of correlation between age and education level, plotting education numerical score (1-16) vs age (1-80) on a scatter plot, with an arbitrary 4 clusters separating those

Attempting to copy/paste from the tabular data:

```ipython
>>> pd.options.display.max_columns = 20
>>> pd.options.display.width = 260
>>> fields = pasted_text.split('\n')
>>> rows = []
... for i in range(int(len(fields)/14)):
...     rows += [fields[i*14:(i+1)*14]]
... df = pd.DataFrame(rows)
>>> df
           0            1               2            3              4            5                 6            7                   8                  9              10           11              12             13
0     Groups  Categorical             age    Numerical  Education-num    Numerical      capital-gain    Numerical        capital-loss          Numerical  hours-per-week    Numerical       workclass    Categorical
1  Education  Categorical  marital-status  Categorical     occupation  Categorical      relationship  Categorical                race        Categorical          gender  Categorical  native-country    Categorical
2    Group 2         50.0            13.0          0.0            0.0         13.0  Self-emp-not-inc    Bachelors  Married-civ-spouse    Exec-managerial         Husband        White            Male  United-States
3    Group 4         38.0             9.0          0.0            0.0         40.0           Private      HS-grad            Divorced  Handlers-cleaners   Not-in-family        White            Male  United-States
4    Group 3         53.0             7.0          0.0            0.0         40.0           Private         11th  Married-civ-spouse  Handlers-cleaners         Husband        Black            Male  United-States
5    Group 4         28.0            13.0          0.0            0.0         40.0           Private    Bachelors  Married-civ-spouse     Prof-specialty            Wife        Black          Female           Cuba
```

## Data Robot

$150k/yr license

## Google AutoML

## Amazon Sagemaker?

## Facebook Prophet
