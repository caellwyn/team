# Agile Machine Learning and Data Science

Most Enterprises don't do Agile ML/DS as well as they could. 
Here's my agile process that really delights stakeholders.

1. Objective Function (Target Variable)
2. Data Extraction (E in ETL)
3. Target Variable Data Wrangling (TL in ETL)
4. Feature Variable Data Wrangling (TL in ETL)
5. Feature Engineering (T in ETL)
6. Modeling
7. Model Validation/Testing
8. Model Interpretation/Reporting
9. Refine Your Objective Function ("Beneficial AI")
10. Go back to step 1 and incrementally improve it


## 1. Objective Function (Target Variable)

Find out what variable in your databases the stakeholders would like to improve.
You and your stakeholders can just take a guess if you don't know what you'd like to predict.
Over time you'll develop an intuition for the kinds of things that are useful to predict in whatever business you are in.

Whether your customers are website visitors, fitbit app users, or healthcare patients, it's almost always best to focus on their satisfaction:

1. user satisfaction (CSAT,[^1] NPS,[^2] CES)[^3]
2. user intake funnel yield (conversion rate)
3. customer churn

And don't forget the employees.
If your employees don't enjoy eating and talking about the "dogfood" you are selling, then your customers probably won't either.
In the startup world this is called "dogfooding" your product.[^4]

1. employee pride in your product (NPS)
2. employee churn
3. voluntary internal product usage (dogfooding) rate 

If you CEO or executives or investors are involved, then you may have to incorporate into your objective function some measure of business success:

1. daily, monthly, quarterly revenue
2. daily, monthly, quarterly revenue change/growth
3. daily, monthly, quarterly profit or profit increase

But this focus on the short term success of your business can often put your companies objectives in opposition to your customers' needs.
This can lead to the decline or stagnation of your business in the long run.
Your competitors that are able to satisfy you users better will likely outcompete you if you focus on monetizing your customer relationship and your customer data.

## 2. Data Extraction (E in ETL)

As a data scientist or machine learning engineer, you shouldn't spend much time thinking about what's important to the business.
If your project involves an RCT, then hypotheses are expensive to test.
And in that case it pays to be careful thinking about your hypotheses.
But that's not Data Science. 
Data Science and Machine Learning are a way to leverage your data and computational power to have machines test hypotheses.
Data Science can help you make smarter decisions about RCTs and protocols for drug trials, etc.
So the sooner you get to a trained model, the sooner you'll start making progress on actionable business or healthcare insights.

In previous decades and in many industries where RCTs are the only opion, hypotheses are important. Many data scientists
The stakeholders will have their own ideas, and it's your job to give them the levers and metrics to test their ideas.
Do **not** waste any time formulating a hypothesis.
Reassure your stakeholders that you're not ignoring their ideas. 
You're just going to give them exponentially more business insight than they could ever get by testing any one hypothesis.
With a modern machine learning pipeline you can test 1000's of hypotheses in the time it takes a team to agree on a single hypothesis to test.

So your first priority after determining a preliminary target variable is to get access to any tables that contain it.
This may mean talking to your stakeholders or IT department to get direct access to the databases you need.

If you're lucky enough to get access to the data you should know about Django and Django Extensions.
The Django ORM (Object Relation Mapping) provides a way for you to access the data in databases with only Python.
And you can even automate much of the data engineering process, such as creating ERDs (Entity Relation Diagrams).

Here's the critical lines of code you need to get a handle on even the most complex database:

```
export "DATABASE_URL=postgres://$DB_USERNAME:$DB_PASSWORD@$DB_IP_ADDRESS:$DB_PORT/$DB_NAME"
python manage.py inspectdb >> models.py


More likely it may mean getting access to some subset of the data in a data **dump**, such as CSV, SQL, or JSON files.
You may even have to scrape your own company's data from their internal dashboards or tools.
No worries this is a cynch

## 3. Target Variable Data Wrangling (the **TL** in E**TL**)

Clean up that variable (remove or interpolate any nonnumerical data, like NaNs or Nulls or empty/missing values)
4. Wrangling: clean up any other easy variable you can find in that same table, (usually a timestamp or datetime)
5. Feature Engineering: extract some numerical value from the variable. also, as a second numerical binary feature create a new binary such as timestamp_isnull that is 0 if you didn't have to do any cleaning or filling of missing values and 1 if you had to clean it up
6. Modeling: use the **two** columns of data that you created in step 5 to create a linear or logistic regression against your target variable. this should take seconds
7. Validation: evaluate how accurate your predictions of your target variable are
8. Model Interpretation/Reporting: think about the results by examining the coefficients of the model and sharing it with stakeholders. stakeholders and business managers can also plan out experiments to do A/B testing, RCTs, etc
9. "Beneficial AI": immediately go back to step 1 and think about whether that is still the best target variable to help your stakeholder or if there's some other formula or objective function you'd like to predict, like maybe revenue_percent_growth + customer_satisfaction_score_percent
10. proceed to steps 2-3 (ETL) to compute and clean up that new target variable (if required)
11. proceed to step 4 (Feature Eng) and if there aren't any more interesting features of your data in the table with the target variable(s) you can join that table with another table in your dataset and clean it up (ETL)
12. proceed through steps 5 etc, at each step the team can think about whether that step can be incrementally improved/tweaked for example:
    12.a. a different feature engineering algorithm
    12.b. a different modeling algorithm
    12.c. a different train/test split for cross validation
    12.d. a better visualization to explain results to stakeholders


[^1]: [Net Promoter Score (NPS)](https://en.wikipedia.org/wiki/Net_Promoter)
[^2]: [Customer Satisfaction Score (CSAT)](https://en.wikipedia.org/wiki/Customer_satisfaction)
[^3]: [Customer Effort Score (CES)](https://en.wikipedia.org/wiki/Customer_success#Metrics_of_Success)
[^4]: [Dogfooding](https://en.wikipedia.org/wiki/Eating_your_own_dog_food)
