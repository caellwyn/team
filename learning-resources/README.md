# TangibleAI Resource Repository

## Table of Contents
1. [Command Line](#command-line)
2. [Conda](#conda)
3. [DigitalOcean](#digitalocean)
4. [Dyshel](#dyshel)
5. [Git](#git)
6. [GitLab](#gitlab)
7. [Hobson](#hobson)
8. [Markdown](#markdown)
9. [TangibleAI](#tangibleAI)
10. [Qary](#qary)


### [Command Line](./commandline/README.md)
[Table of Contents](./commandline/readme.md#table-of-contents)
### [Conda](./conda/README.md)
[Table of Contents](./conda/README.md#table-of-contents)
### [DigitalOcean](./digitalocean/README.md)
[Table of Contents](./digitalocean/README.md#table-of-contents)
### [Dyshel](/dyshel/README.md)
[Table of Contents](./dyshel/README.md#table-of-contents)
### [Git](./git/README.md)
[Table of Contents](./git/README.md#table-of-contents)
### [GitLab](./gitlab/README.md)
[Table of Contents](./gitlab/README.md#table-of-contents)
### [Hobson](./hobson/README.md)
[Table of Contents](./hobson/readme.md#table-of-contents)
### [Markdown](./markdown/README.md)
[Table of Contents](./markdown/readme.md#table-of-contents)
### [TangibleAI](./tangibleai/README.md)
[Table of Contents](./tangibleai/readme.md#table-of-contents)
### [Qary](./qary/README.md)
[Table of Contents](./qary/readme.md#table-of-contents)

### [resources.yml](./resources.yml)

Example yml video schema:

```
-
    title: name of the Video or Document
    url: associated digitalocean url
    duration: approximate duration of video
    authors: contributing authors of the work
    tags:
        - first tag denotes the resource folder (or primary topic) of the video
        - second tag denotes the digital ocean source folder
        - additional tags to denote keywords.
-
```

