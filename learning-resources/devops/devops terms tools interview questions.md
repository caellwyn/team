1. Rate yourself

> Rate yourself on each of these skills from 0 to 5. Use the following 5 star scale:  
>   0: hardly heard of it  
>   1: competent, need help now and then  
>   2: proficient, work independently  
>   3: your peers come to you for advice  
>   4: expert outside your workplace  
>   5: published author, lead developer on popular project 

2. Problem solving

> How would you architect our system?

3. NLP & make/buy decisions

> How would you build a webapp to translate from English into Amharic?

4. Devops

> Tell me about the concept of  "infrastructure as code"?

5. Devops tools

> What is your favorite infrastructure orchestration tool?

- Chef
- Salt Stack
- Ansible
- Puppet Bolt
- Terraform Helm
- Kubernetes
- Docker

6. Security Ops

> Where do you store credentials for your web applications and cloud services?

7. Data Privacy

> How do you manage employee access to customer data

8. Compartmentalization of Data

> How do you stratify data for data privacy

- who needs to access what (privacy+security)
- performance (colocation)
- not sharding for performance

> what is your favorite database monitoring tool. what do you use it for.

- monitoring access/security
- monitoring performance
- monitoring integrity

WAF web application firewall
WPP web protection platform 
IDS/IPS and RASP
RBAC
LDAP

## high profile breaches

> tell me about a recent high profile security breach and what you learned from it

- solarwinds 2020, 1000+ companies
+ master password for solarwinds products

- uber 2017, 57M users
+ aws key in 3rd party sw repo

- brit airways 2018, 400k customers

- Microsoft 100+ large companies
+ LDAP breech

- Mocrooft Hotmail backdoor 
+ blank password? letter e or h for password











