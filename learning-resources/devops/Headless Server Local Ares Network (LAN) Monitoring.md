# Network Monitoring

The vnstat application runs in the background to log IP traffic and can display simple statistics about daily and weekly upload and download data transfer (bits transfered).

#### *`$ history | grep vnstat`*
```console
  359  sudo apt install vnstat
  366  sudo systemctl start vnstat
  367  sudo systemctl enable vnstat
  368  sudo systemctl status vnstat
  419  vnstat -m  # monthly stats
  420  vnstat -d  # daily stats
  448  history | grep vnstat
```

The `iptraf` application provides a menu system and more detailed live informaiton about your network and which IP addresses are currently connected to your server.

#### *`$ history | grep iptraf`*
```console
  364  sudo apt install iptraf
  375  sudo systemctl enable iptraf
  384  sudo iptraf-ng
```

The `iptraf` tool can even force your WiFi and ethernet interfaces into "promiscuous" mode, which will allow it to listen to **all** the traffic on your LAN.
You can't spy on people, but you can tell the IP addresses and domain names of the websites other people are visiting or downloading files from.
So your server can see if there's suspicious outgoing traffic from your network potentially "exfiltrating" data (files on your servers) to hackers outside your network.


