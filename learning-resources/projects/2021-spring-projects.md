# 2021 spring projects

## Aditi - questioner bot (in qary or rasa)

* Torronto

### Week 2

- Squad named entity extraction from answer
- Squad find all sentences with named entity in them
- concatenate relevant sentences to create new shorter context
- create target question in second column
- include squad question ID as 3rd column or index for new dataset
- use NMT to translate from statement to question

## Vish - history tutor bot

* gl: @vbhat1
* TZ: Pacific (CT -2)

### Week 2

- download/scrape some documents about greek history
- question answering on history document

### Week 3

- 2 draft "theater" scripts
- 1 hand-crafted dialog.v2.yml

### Week 4

- add random bot response feature

## Camille - convoscript graph merge

San Diego: -2 hr

### Week 2

- run script_to_dialog()
- compare statements with cosine distance?
- merge 2 scripts

### Week 3

- incremental fastlane DS on fake news dataset
- merge graphs within function that detects a change in what human says and state defined by what the bot says.
- next week will work on dealing with what bot says being repeated in different states

## Bhanu 

TZ: IIT

### Abstractive summarization

Long document abstractive summarization, comparing
* BertSum
* BigBird
* Longformer
And potentially testing tweaks to the model or hyperparameter or preprocessing:
* cherry-picking sentences from long document
* first, last, or first+last paragaphs

### Week 2

- train bertsum on GPU

### Week 3

- write up draft report summarizing BertSum results
- create preprocessors (cherypick, first, last, first+last) and test on CPU 
- run trained Bertsum inference on NLPIA manuscript sections or chapters. 
- run pretrained Longformer inference on NLPIA manuscript sections or chapters. 
- run pretrained Longformer inference on NLPIA manuscript sections or chapters. 

### Week 4

- test training code on CPU for various variations of Longformer
- test code on existing test sets using preprocessors for quantitative comparison to SOTA

## Susanna - rasa or qary chatbot

gl: 
TZ: Pacific

### Week 2

- write out back and forth script for desired application
- write alternative back and forth

### Week 3

- implement in botmock
- test UX with users

### Week 4

## Wamani - django backend for playground.proai.org

Africa (Uganda?): +8 hr

### Week 2

- empty django app
- models.py file with fields for x and y

## Una - Product classifier (NLP)

### Week 2

- linear regression on product rating

### Week 3

- focus on fake news classifier
- numerical features from date
- numerical features from text

## Josh - College course TA and student success predictor

Philadelphia? Maine?: +1 hr

### Week 2

- data transformation to sparse binary time series
