# Final Reports

## 2021 Winter

- [Quote Recommender](billy-quote-recommender.md) by Billy Horn (Winter 2021)
- [Word Map](jon-wordmap.ipynb) by Jon Sundin (Winter 2021)
- [Vaccination Uptake](winston-vaccination-report.docx) by Winston Oakley (Winter 2021)
- [Rap Recommender notebook](Rap_recommender.ipynb) and [report](Uzi_final_report) by Uzi Ikwakor (Winter 2021)
- [Neural Machine Translation](https://gitlab.com/tangibleai/team/-/blob/master/learning-resources/projects/reports/hanna-neural-machine-translation.ipynb) by Hanna Seyoum (Winter 2021)

### 2021 Expert Volunteers

- [Deterministic Dialog Engine (FSM)](https://gitlab.com/tangibleai/qary/-/blob/master/src/qary/skills/quiz.py) by Jose Robins
- [NMT hyperparameter tuning and PyPi package](https://gitlab.com/tangibleai/machine-translation/-/blob/master/README.md)
by Winnie Yeung

## 2020 Fall

- _Your First Rasa Chatbot_ tutorial by Eda Firat
- [Spelling corrector for Question Answering](https://gitlab.com/tangibleai/qary/-/blob/master/src/qary/skills/corrected_qa.py)
